package pascal2;
import java.util.Scanner;
import java.text.DecimalFormat;

public class Exercicio_6 {
	static Scanner tecla = new Scanner(System.in);
	public static void main(String[] args) {
	DecimalFormat valor = new DecimalFormat("00000.00");
		int macas = 0;
		
		System.out.println("Quantidade de macas compradas: ");
		macas = tecla.nextInt();
		
		
		if(macas < 12) {
			System.out.println("Valor da compra: " + valor.format(macas*0.3));
		}else {
			System.out.println("Valor da compra: " + valor.format(macas * 0.25));
	}

}
}
