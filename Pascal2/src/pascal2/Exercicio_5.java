package pascal2;
import java.util.Scanner;

public class Exercicio_5 {
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		int ano = 0;
		
		System.out.println("Ano de nascimento c/ quatro digitos: ");
		ano = tecla.nextInt();
		
		if ((2002 - ano) >= 16) {
			System.out.println("Voce podera votar este ano :-) ");
		}else {
			System.out.println("Voce ainda nao podera votar este ano :-( ");
		}
	}

}
