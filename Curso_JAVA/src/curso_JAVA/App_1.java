package curso_JAVA;

import java.util.Locale;
//import java.text.DecimalFormat;
import java.util.Scanner;
import curso_JAVA.Retangulo;
import curso_JAVA.Funcionario;


public class App_1 {
	static private int num; // Encapsulamento
//	static DecimalFormat text = new DecimalFormat("000 000 000 - 000");
	static Scanner teclado = new Scanner(System.in); // Cria��o de novo objeto do tipo scanner
	static final int MAX = 100;// Constante

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Retangulo retangulo = new Retangulo();
		Funcionario funcionario = new Funcionario();

		System.out.println("MENU");
		System.out.println("1 - Opera��es com retangulo");
		System.out.println("2 - Opera��es com sal�rio de um funcion�rio");
		System.out.println("3 - Opera��es com m�dias escolar");
		System.out.println("4 - Opera��es com membros est�ticos 1");
		num = teclado.nextInt();

		System.out.println("------------------------------------------------------------------------------");

		switch (num) {
		case 1:

			System.out.println("Largura do Retangulo: ");
			retangulo.largura = teclado.nextDouble();
			System.out.println("ALtura do Retangulo: ");
			retangulo.altura = teclado.nextDouble();

			System.out.println("------------------------------------------------------------------------------");
			System.out.println("Retangulo");
			System.out.println("Area do retangulo: " + retangulo.calcularArea());
			System.out.println("Perimetro do retangulo: " + retangulo.calcularPerimetro());
			System.out.println("Diagonal do retangulo: " + retangulo.calcularDiagonal());
			System.out.println("------------------------------------------------------------------------------");
			break;
		case 2:
			System.out.println("Funcion�rio: ");
			funcionario.nome = teclado.nextLine();
			System.out.println("Salario: ");
			funcionario.salarioBruto = teclado.nextDouble();
			System.out.println("Imposto: ");
			funcionario.imposto = teclado.nextDouble();
			System.out.println(funcionario.nome + " - R$ " + funcionario.salarioLiquido());

			System.out.println("Porcentagem do seu aumento: ");
			double porcentagem = teclado.nextDouble();
			funcionario.aumento(porcentagem);
			System.out.println("Valor com o aumento: " + funcionario);
			break;


		
		
	}
		teclado.close();
	}
	}
	
