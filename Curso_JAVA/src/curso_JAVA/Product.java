package curso_JAVA;

public class Product {

	private String name;
	private double price;
	private int quantity;

	public Product() { // construtor
	}

	public Product(String name, double price, int quantity) { // construtor
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public Product(String name, double price) { // construtor
		this.name = name;
		this.price = price;
	}

	// Metodos para acessar e atualizar atributos  

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	
	public int getQuantity() {
		return quantity;
	}

	

	// Metodos

	public double totalValueInStock() {// opera��o que retorna o preco vezes a quantidade
		return price * quantity;
	}

	public void addProducts(int quantity) {
		this.quantity += quantity;
	}

	public void removeProduct(int quantity) {
		this.quantity -= quantity;

	}

	public String toString() {
		return "Product: " + name + " , $ " + price + ", Units:  " + quantity + " , Total: " + totalValueInStock()
				+ ".";
	}
}

/*
 * public void removeProducts(int quantity) { this.quantity -= quantity; }
 * public String toString() { return name + ", $ " + String.format("%.2f",
 * price) + ", " + quantity + " units, Total: $ " + String.format("%.2f",
 * totalValueInStock()); } }
 */