package curso_JAVA;

import java.util.Scanner;

public class Enquanto {

	static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {

		int soma = 0, num = 1, n = 1;

		while (num != 0) {
			System.out.printf("Insira o %d � valor: \n", n);
			num = teclado.nextInt();
			n += 1;
			soma += num;
		}
		System.out.println("A soma dos valores e: " + soma);

	}

}
