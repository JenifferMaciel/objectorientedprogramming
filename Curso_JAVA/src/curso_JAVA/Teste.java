package curso_JAVA;

import java.util.Locale;
import java.util.Scanner;
import curso_JAVA.Product;

public class Teste {

	static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);// Converte para idioma
		//
		System.out.println("Enter product data: ");
		String name = teclado.nextLine();
		System.out.println("Enter product price: ");
		double price = teclado.nextDouble();
		Product product = new Product(name, price); // criando variavel do tipo objeto(Product)
		
		
		product.setName("Computer");
		System.out.println();
		System.out.println("Update name: " + product.getName());// System.out.println(product.toString()); ------> Sobrecarga
		product.setPrice(1200.00);
		
		System.out.println("Product data: " + product);// System.out.println(product.toString()); ------> Sobrecarga

		System.out.println("Enter the number of products to be added in stock: ");
		int quantity = teclado.nextInt();
		product.addProducts(quantity);
		System.out.println("Update data: " + product.getQuantity());

		System.out.println("Enter the number of products to be removed in stock: ");
		quantity = teclado.nextInt();
		product.removeProduct(quantity);
		System.out.println("Update data: " + product.getQuantity());
		teclado.close();

		/*
		 * Revis�o da aula de POO
		 * 
		 * int num; //Atributo num = 0; //Atribuindo o valor 0 (zero) a variavel num
		 * 
		 * System.out.println("Ol� Mundo");// Sa�da de Dados
		 * System.out.println("Insira um primeiro valor: "); num = teclado.nextInt();
		 * System.out.printf("Valor: %d",num);
		 */
		/*
		 * Locale.setDefault(Locale.US); Product product = new Product();
		 * 
		 * System.out.println("Enter product data: "); System.out.print("Name: ");
		 * product.name = teclado.nextLine(); System.out.print("Price: "); product.price
		 * = teclado.nextDouble(); System.out.print("Quantity in stock: ");
		 * product.quantity = teclado.nextInt(); System.out.println();
		 * System.out.println("Product data: " + product); System.out.println();
		 * System.out.print("Enter the number of products to be added in stock: "); int
		 * quantity = teclado.nextInt(); product.addProducts(quantity);
		 * System.out.println(); System.out.println("Updated data: " + product);
		 * System.out.println();
		 * System.out.print("Enter the number of products to be removed from stock: ");
		 * quantity = teclado.nextInt(); product.removeProducts(quantity);
		 * System.out.println(); System.out.println("Updated data: " + product);
		 * 
		 * teclado.close(); //Quando o scanner n�o estiver mais sendo utilizado
		 */

	}

}
