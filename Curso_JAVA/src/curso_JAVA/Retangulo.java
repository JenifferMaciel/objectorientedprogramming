package curso_JAVA;

public class Retangulo {
	
		public double altura;
		public double largura;
		public double diagonal, area, perimetro;
		
		
		public double calcularArea(){
		return largura * altura;
			
		}
		
		public double calcularPerimetro(){
		return (altura + largura)*2;
		}
		
		public double calcularDiagonal(){
		return Math.sqrt(largura * largura + altura * altura);
		}

}

