public class Poupanca extends Conta {

	private double rendimento;

	public Poupanca(int numero, double saldo) {
		super(numero, saldo);
		this.rendimento = 0;
	}

	public void depositar(double valor)// obs: override � opcional,e esta acontecendo uma sobrescrita//
	{
		setSaldo(getSaldo() + valor);
		this.rendimento += getSaldo() * 0.5;
	}

	public double getRendimento() {
		return rendimento;
	}

}
