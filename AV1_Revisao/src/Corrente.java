public class Corrente extends Conta {
	// atributo novo
	private double limite;

	// construtor classe conta corrente METODO
	public Corrente(int numero, double saldo, double limite) {
		super(numero, saldo);
		this.limite = limite;
	}

	// sobrescrita de m�todo
	// @override
	public void sacar(double valor, int v)// esta fazendo sobrescrita? N�o,mas compila com dois m�todos//
	{
		if (valor <= getSaldo()) {
			setSaldo(getSaldo() - valor);
		} else {
			System.out.println(" ");
		}
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}
}