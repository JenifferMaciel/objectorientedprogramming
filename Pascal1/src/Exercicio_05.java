import java.util.Scanner;
import java.text.DecimalFormat;

public class Exercicio_05 {

	static Scanner tecla = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		double odom_i, odom_f, litros, valor_t, media, lucro, gasol_l;
		DecimalFormat medias = new DecimalFormat("0000.0");
		DecimalFormat valor = new DecimalFormat("00000000.00");
		//{odom_i: odometro inicial
		//odom_f: odometro final
		//valor_t: valor total
		//gasol_l: valor do litro da gasolina}
		
		odom_i = 0;
		odom_f = 0;
		litros = 0;
		valor_t = 0;
		media = 0;
		lucro = 0;
		gasol_l = 1.90;
		
	
		System.out.println("Marcacao inicial do odometro (Km):");
		odom_i = tecla.nextDouble();
		
		System.out.println("Marcacao final do odometro (Km):");
		odom_f = tecla.nextDouble();
		
		System.out.println("Quantidade de combustivel gasto (litros): ");
		litros = tecla.nextDouble();
		
		System.out.println("Valor total recebido (R$): ");
		valor_t = tecla.nextDouble();
		
		media = (odom_f - odom_i) / litros;
		lucro = valor_t - (litros * gasol_l);
	
		System.out.println("Media de consumo em Km/L: " + medias.format(media));
	
		System.out.println("Lucro (liquido) do dia: R$ " + valor.format(lucro));
	
	


	}

}
