import java.util.Scanner;

public class Exercicio_03 {
	
 static Scanner tecla = new Scanner(System.in);
 
	public static void main(String[] args) {
	
		double pot_lamp, larg_com, comp_com, area_com, pot_total, div; // 
		int num_lamp=0;
		//pot_lamp: potencia da lampada
		//larg_com: largura do comodo
		//comp_com: comprimento do comodo
		//area_com: area do comodo
		//pot_total: potencia total
		//num_lamp: numero de lampadas
	
		System.out.println("Qual a potencia da lampada (em watts)?");
		pot_lamp = tecla.nextDouble();
		System.out.println("Qual a largura do comodo (em metros)? ");
		larg_com = tecla.nextDouble();
		System.out.println("Qual o comprimento do comodo (em metros)?");
		comp_com = tecla.nextDouble();
		
		area_com = larg_com * comp_com;
		pot_total = area_com * 18;
		div=pot_total/pot_lamp;
		num_lamp= (int)div;
	
		System.out.println("Numero de lampadas necessarias para iluminar esse comodo: " + num_lamp);
		
	}

}
