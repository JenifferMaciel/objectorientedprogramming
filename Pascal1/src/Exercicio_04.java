import java.util.Scanner;

public class Exercicio_04 {

	static Scanner tecla = new Scanner(System.in);
	public static void main(String[] args) {
		
		double comp, larg, alt, area;
		int caixas;
		
		comp = 0; //{inicializar variaveis}
		larg = 0;
		alt = 0;
		area = 0;
		caixas = 0;
		
		System.out.println("Qual o comprimento da cozinha?");
		comp = tecla.nextDouble();
		
		System.out.println("Qual a largura da cozinha? ");
		larg = tecla.nextDouble();
		
		System.out.println("Qual a altura da cozinha? ");
		alt = tecla.nextDouble();
		
		
		area = (comp*alt*2) + (larg*alt*2);
		caixas = (int)(area/1.5); 
		
		System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: " + caixas);


	}

}
