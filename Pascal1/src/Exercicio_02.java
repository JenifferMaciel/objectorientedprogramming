import java.util.Scanner;
import java.text.DecimalFormat;

public class Exercicio_02 {
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		DecimalFormat df = new DecimalFormat("0.00");
		double temp_f, temp_c;
		
		temp_f = 0;
		temp_c = 0;
		
		System.out.println("Informe a temperatura em graus Fahrenheit: ");
		temp_f = tecla.nextDouble();
		temp_c = ((temp_f -32)*5)/9;
		System.out.println("A temperatura em graus Celsius eh: " + df.format(temp_c));

	}

}
