import java.util.Scanner;

public class Exercicio_01  {
	
	//Declarao de variveis globais (static)
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		//Declaracao de variveis locais e constantes
		double raio, area;
		final double PI = 3.14;
		
		//Entrada de dados
		System.out.println("Digite o valor do raio: ");
		raio = tecla.nextDouble();
		
		//Processamento de dados
		area = PI * Math.pow(raio,2);
		
		//area = Math.PI * Math.pow(raio,2);
		
		//Saida da informacao
		System.out.println(": " + area);
	}
}